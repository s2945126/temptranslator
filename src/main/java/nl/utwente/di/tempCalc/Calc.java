package nl.utwente.di.tempCalc;

public class Calc {

    public double getFahrenheit(int celcius) {
        return (celcius * (9.0f/5)) + 32;
    }
}
