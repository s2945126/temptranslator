package nl.utwente.di.tempCalc;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;


public class TempCalc extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Calc calc;
	
    public void init() throws ServletException {
    	calc = new Calc();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius to Fahrenheit";

    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("temp") + "\n" +
                "  <P>Fahrenheit: " +
                   calc.getFahrenheit(Integer.parseInt(request.getParameter("temp"))) +
                "</BODY></HTML>");
  }
}
